remote access for user  {{ pillar.abraham_user.username }}:
  ssh_auth.manage:
    - user: {{ pillar.abraham_user.username }}
    - ssh_keys:
{% for key in pillar.abraham_user.ssh_access %}
      - {{ key }}
{% endfor %}
