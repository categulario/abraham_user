# -----------------------------------------------------------------------------+
# General dotfiles for any managed system
# -----------------------------------------------------------------------------+
{% for dotfile in ['fish', 'nvim', 'git', 'tmux', 'alacritty', 'tiempo'] %}
/home/{{ pillar.abraham_user.username }}/.config/{{ dotfile }}:
  file.recurse:
    - source: salt://abraham_user/src/files/dots/{{ dotfile }}
    - user: {{ pillar.abraham_user.username }}
    - group: {{ pillar.abraham_user.username }}
    - makedirs: true
    - template: jinja
{% endfor %}

/home/{{ pillar.abraham_user.username }}/.psqlrc:
  file.managed:
    - source: salt://abraham_user/src/files/dots/psqlrc
    - user: {{ pillar.abraham_user.username }}
    - group: {{ pillar.abraham_user.username }}

curl for vim plug:
  pkg.installed:
    - pkgs:
      - curl

vim plug:
  cmd.run:
    - name: curl -fLo /home/{{ pillar.abraham_user.username }}/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    - runas: {{ pillar.abraham_user.username }}
    - creates:
      - /home/{{ pillar.abraham_user.username }}/.local/share/nvim/site/autoload/plug.vim
