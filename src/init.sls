# -----------------------------------------------------------------------------+
# Set my manager user in any system
#
# This ensures all necessary packages and settings for a comfortable and
# familiar management experience in any new system.
#
# ## Pillar
#
# abraham_user:
#   username: abraham
#   password: (mkpasswd -m sha-256)
#   extra_groups:
#     - www-data
#   dirs:
#     - tmp
#   ssh_access:
#     - ssh-rsa ...
# -----------------------------------------------------------------------------+
shell package:
  pkg.installed:
    - pkgs:
      - fish

# create password with:
# mkpasswd -m sha-256
# provided by package whois
{{ pillar.abraham_user.username }} user account:
  user.present:
    - name: {{ pillar.abraham_user.username }}
    - shell: {{ pillar.abraham_user.shell|default('/bin/bash') }}
    - home: /home/{{ pillar.abraham_user.username }}
{% if 'password' in pillar.abraham_user %}
    - password: {{ pillar.abraham_user.password }}
{% endif %}
    - remove_groups: false
    - optional_groups:
      - adm
      - sudo
      - systemd-journal
{% for group in pillar.abraham_user.extra_groups|default([]) %}
      - {{ group }}
{% endfor %}

# Some common directories (without contents)
{% for dir in pillar.abraham_user.dirs|default([]) %}
/home/{{ pillar.abraham_user.username }}/{{ dir }}:
  file.directory:
    - user: {{ pillar.abraham_user.username }}
    - group: {{ pillar.abraham_user.username }}
    - makedirs: true
    - mode: 755
{% endfor %}
