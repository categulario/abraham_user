syntax on
syntax enable

" Automatically install vim-plug
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
    silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(stdpath('data') . '/plugged')

Plug 'ctrlpvim/ctrlp.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'henrik/vim-indexed-search'
Plug 'JulesWang/css.vim'
Plug 'cakebaker/scss-syntax.vim'
Plug 'vim-airline/vim-airline'
Plug 'posva/vim-vue'
Plug 'cespare/vim-toml'
Plug 'lambdalisue/suda.vim'
Plug 'ap/vim-css-color'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'tpope/vim-fugitive'
Plug 'chemzqm/vim-jsx-improve'
Plug 'jremmen/vim-ripgrep'
Plug 'dhruvasagar/vim-table-mode'
Plug 'elixir-editors/vim-elixir'
Plug '~/src/escribe/escribe.vim'
Plug 'khaveesh/vim-fish-syntax'
Plug 'vmware-archive/salt-vim'
Plug 'jwalton512/vim-blade'

Plug 'humanoid-colors/vim-humanoid-colorscheme'
Plug 'schickele/vim-fruchtig'
Plug 'liuchengxu/space-vim-theme'
Plug 'NLKNguyen/papercolor-theme'

call plug#end()

" Theme things
" (test first in servers, because some themes need newer features not present in
" ubuntu deployments
set termguicolors     " enable true colors support
set background=dark
"set background=dark
"colorscheme humanoid
"colorscheme fruchtig
colorscheme humanoid

autocmd Filetype html setlocal tabstop=2
autocmd Filetype html setlocal softtabstop=2
autocmd Filetype html setlocal shiftwidth=2

autocmd Filetype htmldjango setlocal tabstop=2
autocmd Filetype htmldjango setlocal softtabstop=2
autocmd Filetype htmldjango setlocal shiftwidth=2

autocmd Filetype vue setlocal tabstop=2
autocmd Filetype vue setlocal softtabstop=2
autocmd Filetype vue setlocal shiftwidth=2

autocmd Filetype xml setlocal tabstop=2
autocmd Filetype xml setlocal softtabstop=2
autocmd Filetype xml setlocal shiftwidth=2

autocmd Filetype svg setlocal tabstop=2
autocmd Filetype svg setlocal softtabstop=2
autocmd Filetype svg setlocal shiftwidth=2

autocmd Filetype rng setlocal tabstop=2
autocmd Filetype rng setlocal softtabstop=2
autocmd Filetype rng setlocal shiftwidth=2

autocmd Filetype rst setlocal tabstop=3
autocmd Filetype rst setlocal softtabstop=3
autocmd Filetype rst setlocal shiftwidth=3

autocmd Filetype javascript setlocal tabstop=2
autocmd Filetype javascript setlocal softtabstop=2
autocmd Filetype javascript setlocal shiftwidth=2
autocmd Filetype javascript setlocal keywordprg=helpmdn

autocmd Filetype typescript setlocal tabstop=2
autocmd Filetype typescript setlocal softtabstop=2
autocmd Filetype typescript setlocal shiftwidth=2
autocmd Filetype typescript setlocal keywordprg=helpmdn

autocmd Filetype javascriptreact setlocal tabstop=2
autocmd Filetype javascriptreact setlocal softtabstop=2
autocmd Filetype javascriptreact setlocal shiftwidth=2
autocmd Filetype javascriptreact setlocal keywordprg=helpmdn

autocmd Filetype yaml setlocal tabstop=2
autocmd Filetype yaml setlocal softtabstop=2
autocmd Filetype yaml setlocal shiftwidth=2

autocmd Filetype r setlocal tabstop=2
autocmd Filetype r setlocal softtabstop=2
autocmd Filetype r setlocal shiftwidth=2

autocmd Filetype php setlocal tabstop=4
autocmd Filetype php setlocal softtabstop=4
autocmd Filetype php setlocal shiftwidth=4
autocmd Filetype php setlocal keywordprg=helpphp

autocmd Filetype blade setlocal tabstop=2
autocmd Filetype blade setlocal softtabstop=2
autocmd Filetype blade setlocal shiftwidth=2

autocmd Filetype lisp setlocal tabstop=2
autocmd Filetype lisp setlocal softtabstop=2
autocmd Filetype lisp setlocal shiftwidth=2

autocmd Filetype css setlocal tabstop=4
autocmd Filetype css setlocal softtabstop=4
autocmd Filetype css setlocal shiftwidth=4
autocmd Filetype css setlocal iskeyword+=-

autocmd Filetype scss setlocal tabstop=2
autocmd Filetype scss setlocal softtabstop=2
autocmd Filetype scss setlocal shiftwidth=2
autocmd Filetype scss setlocal iskeyword+=-
autocmd Filetype scss setlocal iskeyword+=$

autocmd Filetype rust setlocal makeprg=cargo\ run
autocmd Filetype rust setlocal keywordprg=helprust

autocmd Filetype cpp setlocal makeprg=g++\ --std=c++11\ %\ &&\ ./a.out<in.txt

autocmd Filetype python setlocal makeprg=python\ %
autocmd Filetype python setlocal keywordprg=helppython

autocmd Filetype text setlocal formatoptions+=t
autocmd Filetype rst setlocal formatoptions+=t

set expandtab
set nomodeline
set incsearch
set tabstop=4
set softtabstop=4
set shiftwidth=4
set ignorecase
set smartcase
set showmatch
set hlsearch
set relativenumber
set number
set linebreak
set grepprg=git\ grep\ -n
set laststatus=2
set efm+=%f:%l
set updatetime=250
set cursorline
set listchars=space:·,tab:»\ 
set list
set scrolloff=10
set directory=~/.config/nvim/swapfiles//
set colorcolumn=80,100,120
set keywordprg=firevim
set hidden
set signcolumn=yes
set inccommand=nosplit
set switchbuf=useopen
set textwidth=80
{% if grains.os == 'Ubuntu' %}
set formatoptions=jcroql
{% else %}
set formatoptions=jcroq/l
{% endif %}

highlight ColorColumn ctermbg=8

let $BASH_ENV = "~/.bash_aliases"
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']
let mapleader = ','
let g:netrw_liststyle = 3
let g:netrw_banner = 0
let g:user_emmet_expandabbr_key = '<C-a>,'
let g:suda#prefix = 'sudo://'
let g:suda_smart_edit = 1

let $RUST_BACKTRACE = 1

nnoremap <C-J> <C-W>j
nnoremap <C-K> <C-W>k
nnoremap <C-L> <C-W>l
nnoremap <C-H> <C-W>h
nnoremap <Space> @@

nnoremap <leader>U gUiw
nnoremap <leader>u guiw
nnoremap <leader>ev :tabe $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
nnoremap <leader>w :call TrimWhitespace()<CR>
nnoremap <leader>f :call FormatCode()<CR>
nnoremap <leader><leader> :noh<cr>
nnoremap <leader>n :cnf<cr>
nnoremap <leader>d ggVGd
nnoremap <C-o> :tabe<cr>
nnoremap <C-f> :CocSearch <C-r>/<cr>
nnoremap <f5> :make<cr>

vnoremap <leader>q :normal @q<CR>
vnoremap <leader>p :call PasteBinSel()<cr>
nnoremap <leader>p :call PasteBinFile()<cr>

" exit from terminal mode with esc
tnoremap <Esc> <C-\><C-n>

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

nnoremap <silent> K :call <SID>show_documentation()<CR>

" Symbol renaming.
nmap <f2> <Plug>(coc-rename)
" Remap keys for applying codeAction to the current line.
nnoremap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nnoremap <leader>qf  <Plug>(coc-fix-current)
nnoremap <leader>t m'A<C-R>=strftime('%Y-%m-%d %H:%M')<CR><Esc>``

" coc-git
nmap [c <Plug>(coc-git-prevchunk)
nmap ]c <Plug>(coc-git-nextchunk)
nmap <leader>hp <Plug>(coc-git-chunkinfo)
nmap <leader>hu :CocCommand git.chunkUndo<cr>

map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

command! H1 call H1()
command! H2 call H2()
command! H3 call H3()
command! Date call InsertDate()

function! InsertDate()
    :read! date +'\%Y-\%m-\%d \%H:\%M'
    :normal kJ
endfunction

function! H1()
    :normal yypVr=
endfunction

function! H2()
    :normal yypVr-
endfunction

function! H3()
    :normal yypVr.
endfunction

function! TrimWhitespace()
    :normal qaq
    :normal ma
    :%s/\s\+$//g
    :normal 'a
endfunction

function! FormatCode()
    :normal qaqmaggVG='a
endfunction

function! PasteBinFile()
    :redir @+>
    :w !curl -s --data-binary @- https://paste.rs/ | xargs
    :redir END
endfunction

function! PasteBinSel() range
    let n = @n
    silent! normal gv"ny
    :redir @+>
    echo system("echo '" . @n . "' | curl -s --data-binary @- https://paste.rs/ | xargs")
    :redir END
    let @n = n
    normal! gn
endfunction
