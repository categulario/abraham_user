function e
    tmuxp load ({% if grains.os_family == 'Suse' %}eza{% else %}exa{% endif %} -1 ~/.config/tmuxp/ | sed 's/.yaml$//g' | fzf)
end
