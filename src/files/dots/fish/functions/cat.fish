function cat
    {% if grains.os_family == 'Debian' %}batcat{% else %}bat{% endif %} --theme 1337 -p $argv
end
