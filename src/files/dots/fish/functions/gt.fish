function gt
    git tag --format "%(refname:strip=2) %(contents:subject)" --sort version:refname | tail -n10
end
