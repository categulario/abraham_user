function myip --wraps='curl -s https://httpbin.org/get | jq -r .origin'
curl -s https://httpbin.org/get | jq -r .origin
end
