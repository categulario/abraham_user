function hack
    set -l name (string split '/' (pwd) | tail -n1)
    tmux \
        new-session -s $name -n shell ';' \
        send-keys 'git pull' 'Enter' ';' \
        new-window -n code ';' \
        send-keys 'nvim' 'Enter'
end
