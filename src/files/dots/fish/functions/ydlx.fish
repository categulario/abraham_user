function ydlx
{% if grains.host == 'be-quiet-suse' %}
    cd /mnt/datos/abraham/Música/Colección\ personal && yt-dlp -x --audio-format mp3 "https://www.youtube.com/watch?v=$argv[1]" $argv[2..]
{% else %}
    ssh be-quiet ydlx $argv
{% endif %}
end
