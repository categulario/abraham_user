function password
    set -q argv[2]; and set length $argv[2]; or set length 16

    if test $argv[1] = 'strong'
        cat /dev/urandom | tr -dc 'a-zA-Z0-9-_!@#$%^&*()_+{}|:<>?=' | fold -w $length | head -n 1 | grep -i '[!@#$%^&*()_+{}|:<>?=]' | xclip -sel clip
    else
        cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $length | head -n 1 | xclip -sel clip
    end
end
