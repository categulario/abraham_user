function authors
    git log --format=short | grep Author | sort | uniq
end
