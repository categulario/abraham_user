function upload
    set -l path $argv[1]

    if test -z $path
        echo 'Pass the path to a file to upload it'
        return 1
    end

    curl --fail --upload-file $path --user categulario https://categulario.xyz/upload/

    if string match --quiet --entire '/' $path
        set -l parts (string split --right --max 1 '/' $path)
        set -l filename $parts[2]

        echo "https://categulario.xyz/d/$filename"
    else
        echo "https://categulario.xyz/d/$path"
    end
end
