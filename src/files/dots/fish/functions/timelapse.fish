function timelapse
    if test -z $argv[1]
        echo "Necesita como primer argumento una ruta"
        exit 1
    end

    set -f ruta $argv[1]
    set -f start (string sub -s 4 -l 5 (eza $ruta | head -n 1))
    set -f end (string sub -s 4 -l 5 (eza $ruta | tail -n 1))
    set -f count (math $end - $start)

    ffmpeg -start_number $start -i "$ruta/G00%5d.JPG" -vframes $count -c:v libx264 -r 24.97 -pix_fmt yuv420p $ruta/salida.mp4
end
