fish_vi_key_bindings

set -gx EDITOR nvim
set -gx PAGER less
set -gx PIPENV_VENV_IN_PROJECT 1
set -gx PATH $HOME/.local/bin $PATH

{% if grains.environment|default('server') == 'desktop' %}
set -gx RUSTUP_HOME $HOME/.local/opt/rustup
set -gx CARGO_HOME $HOME/.local/opt/cargo
set -gx PATH $CARGO_HOME/bin $PATH
{% endif %}

{% if grains.environment|default('server') == 'desktop' %}
set -g fish_greeting ""
{% else %}
set -g fish_greeting "Estás en un servidor remoto"
{% endif %}

set -g __fish_git_prompt_showupstream informative
set -g __fish_git_prompt_char_upstream_ahead '@'
set -g __fish_git_prompt_showdirtystate 1
set -g __fish_git_prompt_showuntrackedfiles 1
set -g __fish_git_prompt_showcolorhints 1
set -g __fish_git_prompt_color yellow
set -g __fish_git_prompt_color_branch yellow

set -g __fish_git_prompt_char_stagedstate '*'
set -g __fish_git_prompt_color_stagedstate green
set -g __fish_git_prompt_char_dirtystate '*'
set -g __fish_git_prompt_color_dirtystate yellow
set -g __fish_git_prompt_showstashstate 1
set -g __fish_git_prompt_char_stashstate 's'
set -g __fish_git_prompt_color_stashstate blue
set -g __fish_git_prompt_char_untrackedfiles '*'
set -g __fish_git_prompt_color_untrackedfiles red
set -g fish_color_host cyan

{% if grains.os_family == 'Suse' %}
{% set exa = 'eza' %}
{% else %}
{% set exa = 'exa' %}
{% endif %}

abbr -a ls {{ exa }}
abbr -a l {{ exa }}
abbr -a la {{ exa }} -lahg
abbr -a tree {{ exa }} --tree
abbr -a vim nvim
abbr -a vi nvim
abbr -a prunit podman run -it --rm
abbr -a tattach tmux attach -t
abbr -a userctl systemctl --user
abbr -a userjournal journalctl --user
{% if grains.environment|default('server') == 'desktop' %}
abbr -a trans_en trans es:en
abbr -a trans_es trans en:es
abbr -a tres trans tr:es
abbr -a serve miniserve -p 8000 --index index.html .
{% endif %}

if status is-interactive
    if test -e $HOME/.local/opt/asdf/asdf.fish
        source $HOME/.local/opt/asdf/asdf.fish
    end
end
