# -----------------------------------------------------------------------------+
# Command line utilities that I want in every server and desktop
#
# The first list is packages that have the same name in arch and ubuntu
# -----------------------------------------------------------------------------+
basic packages:
  pkg.installed:
    - pkgs:
      - nmap
      - ripgrep
      - unzip
      - bat
      - neovim
      - tmux
      - git
      - whois # provides mkpasswd
      - htop
      - rsync

# -----------------------------------------------------------------------------+
# nslookup and dig
# -----------------------------------------------------------------------------+
dnsutils:
  pkg.installed:
    - pkgs:
{% if grains.os_family == 'Debian' %}
      - dnsutils # nslookup, dig
{% elif grains.os_family == 'Arch' %}
      - bind # nslookup, dig
{% elif grains.os_family == 'Suse' %}
      - bind-utils
{% endif %}

# -----------------------------------------------------------------------------+
# fd (find replacement)
# -----------------------------------------------------------------------------+
fd package:
  pkg.installed:
    - pkgs:
{% if grains.os_family in ['Arch', 'Suse'] %}
      - fd
{% else %}
      - fd-find
{% endif %}

# -----------------------------------------------------------------------------+
# duf
# -----------------------------------------------------------------------------+
{% if
    grains.os_family in ['Arch', 'Suse'] or
    (grains.os == 'Ubuntu' and grains.osrelease >= '22.04') or
    (grains.os == 'Debian' and grains.osrelease >= '12')
%}
duf from package:
  pkg.installed:
    - pkgs:
      - duf
{% elif grains.os_family == 'Debian' %}
duf from source:
  pkg.installed:
    - sources:
      - duf: https://github.com/muesli/duf/releases/download/v0.8.0/duf_0.8.0_linux_amd64.deb
{% endif %}

# -----------------------------------------------------------------------------+
# dust
# -----------------------------------------------------------------------------+
{% if grains.os_family in ['Arch', 'Suse'] %}
dust from package:
  pkg.installed:
    - pkgs:
      - dust
{% else %}
dust from source:
  pkg.installed:
    - sources:
      - du-dust: https://github.com/bootandy/dust/releases/download/v0.8.3/du-dust_0.8.3_amd64.deb
{% endif %}

# -----------------------------------------------------------------------------+
# exa
# -----------------------------------------------------------------------------+
{% if
    grains.os_family in ['Arch', 'Suse'] or
    (grains.os == 'Ubuntu' and grains.osrelease >= '22.04') or
    (grains.os == 'Debian' and grains.osrelease >= '12')
%}
exa from package:
  pkg.installed:
    - pkgs:
{% if grains.os_family in ['Arch', 'Suse'] or (grains.os == 'Ubuntu' and grains.osrelease >= '24.04') %}
      - eza
{% else %}
      - exa
{% endif %}
{% endif %}

{% if grains.os == 'Ubuntu' and grains.osrelease == '20.04' %}
exa for ubuntu 20:
  cmd.run:
    - name: curl -L https://github.com/ogham/exa/releases/download/v0.10.0/exa-linux-x86_64-v0.10.0.zip -o /home/{{ pillar.abraham_user.username }}/.local/exa.zip --create-dirs && unzip /home/{{ pillar.abraham_user.username }}/.local/exa.zip -d /home/{{ pillar.abraham_user.username }}/.local/ && rm /home/{{ pillar.abraham_user.username }}/.local/exa.zip
    - runas: {{ pillar.abraham_user.username }}
    - creates:
      - /home/{{ pillar.abraham_user.username }}/.local/bin/exa
{% endif %}

# -----------------------------------------------------------------------------+
# asdf will handle node versions
# -----------------------------------------------------------------------------+
https://github.com/asdf-vm/asdf.git:
  git.latest:
    - target: /home/{{ pillar.abraham_user.username }}/.local/opt/asdf
    - rev: v0.14.1
    - force_fetch: true
    - force_reset: true
    - user: {{ pillar.abraham_user.username }}

/home/{{ pillar.abraham_user.username }}/.config/fish/completions/asdf.fish:
  file.symlink:
    - target: /home/{{ pillar.abraham_user.username }}/.local/opt/asdf/completions/asdf.fish
    - user: {{ pillar.abraham_user.username }}
    - group: {{ pillar.abraham_user.username }}
    - makedirs: true
