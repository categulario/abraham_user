postgresql running for abraham_user:
  service.running:
    - name: postgresql

my user:
  postgres_user.present:
    - name: {{ pillar.abraham_user.username }}
    - login: true
    - superuser: true
    - require:
      - service: postgresql running for abraham_user

my database:
  postgres_database.present:
    - name: {{ pillar.abraham_user.username }}
    - encoding: UTF8
    - owner: {{ pillar.abraham_user.username }}
    - require:
      - postgres_user: my user
