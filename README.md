# Mis dotfiles

Configuraciones y paquetes que uso para manejar sistemas. Tanto mis escritorios
como servidores.

Sirve incluyéndolo en un repositorio salt más grande.

## Cómo usar

Añadir como submódulo a un repositorio que represente un proyecto salt, como:

    git submodule add https://gitlab.com/categulario/abraham_user.git src/abraham_user

Luego añadir a `top.sls` en el host adecuado:

    - abraham_user/src/init
    - abraham_user/src/packages
    - abraham_user/src/dots

según se requiera. Luego añadir a ese host un pilar como el siguiente:

```yaml
abraham_user:
    username: abraham
    password: $5$blablablablablablablablablablablablablabla
    extra_groups:
        - www-data
    dirs:
        - tmp
    ssh_access:
        - ssh-rsa AAA.ferrnfuiernfernfiurenfieurn...
```

Los grupos se añadirán al usuario y los directorios se crearán dentro de
`/home/{{ username }}`. Las llaves ssh tendrán acceso. Para generar el password
se usa el comando `mkpasswd`:

    mkpasswd -m sha-256
